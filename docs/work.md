### Процесс установки AlpineOS

Начнем с загрузки образа системы для ее дальнейшей установки в виртуальную машину.
Выбираем образ для загрузки в ВМ.

![alpine_1](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/alpine_1.png)

Запускаем систему.

Встречает нас такое окно:

![apline_2](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/apline_2.png)

Заходим под рутом для продолжения работы и начала установки

![alpine_3](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/alpine_3.png)

Вводим команду 
```
setup-alpine
```
и начинаем установку.

Система предлагает выбрать языковую раскладку. Пишем "us" для англ.языка и идем дальше...

![alpine_4](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/alpine_4.png)

Пишем имя пользователя:

![alpine_5](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/alpine_5.png)

Далее предлагается выбрать сетевой интерфейс. Из доступных есть только стандартный eth0.

![alpine_6](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/alpine_6.png)

Предлагается настроить IP адрес для данного сетевого интерфейса. Из доступных только DHCP протокол. Но можно его на данный момент оставить без настройки.

![alpine_7](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/alpine_7.png)

Вводим пароль для системы

![alpine_8](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/alpine_8.png)

Установка прокси для протоков HTTP/FTP.

![alpine_9](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/alpine_9.png)

Выбор протокола для синхронизации часов компьютера. Выбираем стандартный chrony

![alpine_10](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/alpine_10.png)

Далее предлагается список зеркал для загрузки системы. Можно выбрать рандомный из списка, с самым быстрым подключением, либо отредактировать файл репозиториев.

![alpine_11](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/alpine_11.png)

Ssh сервер. Из стандартных на выбор OpenSSH либо Dropbear. Выбираем OpenSSH, так как Alpine позиционируется как экономичная система, а DropBear потребляет больше ОЗУ, чем OpenSSH

![alpine_12](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/alpine_12.png)

Выбор дискового накопителя. Т.к мы в виртуальной машине - то выбор только sda.

![alpine_13](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/alpine_13.png)

Установка системы: стандартная установка "sys" - файловая система, ядро и загрузчик будут установлены на жесткий диск; "data" - файловая система будет установлена на диск,
но система будет запускаться из оперативной памяти. "LVM" - позволяющая использовать разные области физического жесткого диска или разных жестких дисков как один логический том.
Выбераем стандартную установку "sys"

Подверждаем и продолжаем.

![alpine_14](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/alpine_14.png)

Далее система спрашивает где хранить конфиги, указываем "floppy". 
В итоге выполняется вход в систему.

![alpine_15](https://gitlab.com/narchuk96/alpineos/raw/master/docs/images/alpine_15.png)

Установка пакетов выполняется с помощью команды 
```
apk add
```






